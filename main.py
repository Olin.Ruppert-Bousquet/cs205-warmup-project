import sqlite3
import csv


# function to query database
# takes input of list of command list
def query_db(input_list):
    # if length is 1, connection must be made
    if len(input_list) == 1:
        case = "connect"
    else:
        try:
            # If length is 2, must be either counting authors or books
            if len(input_list) == 2:
                case = "count"
                query = "SELECT {} FROM {}".format("*", input_list[1])
            else:
                query = 'SELECT {} FROM {} WHERE {}="{}"'.format(input_list[0], input_list[1], input_list[2], input_list[3])
                # sample query
                # query = "SELECT fld_author FROM books WHERE fld_book='1984'"
                if len(input_list) == 4:
                    # only one query to the database
                    case = "single_q"
                else:
                    # two queries to the database
                    case = "double_q"
        except IndexError:
            return
    try:
        # creates connection to database
        if case == "connect":
            global conn, c
            conn = sqlite3.connect('books_and_authors.db')
            c = conn.cursor()
            return

        cnt_lst = []
        # prints out results of a single query
        no_result = True
        for row in c.execute(query):
            no_result = False
            if case == "count":
                cnt_lst.append(row)
            if case == "single_q":
                print(row[0])
            else:
                search_fld = row[0]
        if no_result:
            print("No results for input query")
            return
        if case == "count":
            print("There are {} {}".format(str(len(cnt_lst)), input_list[1]))
        # for a double query where first search acts as a key for the second search
        no_result = True
        if case == "double_q":
            query = 'SELECT {} FROM {} WHERE {}="{}"'.format(input_list[4], input_list[5], input_list[6], search_fld)
            for row in c.execute(query):
                no_result = False
                print(row[0])
            if no_result:
                print("No results for input query")
                return
    # exception for if query fails
    except sqlite3.DatabaseError:
        print("Error. Could not retrieve data.")


def db_quit():
    # closes connection to database
    try:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()
    except:
        print("No database connection made")


# creates the database
# takes no input
def create_db():
    con = sqlite3.connect("books_and_authors.db")
    cur = con.cursor()
    # creates table for all input for books from the csv file
    try:
        b_file = open("50 books.csv")
        a_file = open("authors.csv")
    except FileNotFoundError:
        print("One of more the CSV files requested does not exist")
        exit()
    cur.execute('''CREATE TABLE Books(Book varchar(255), Author varchar(255), Publication int, Pages int)''')
    con.commit()
    rows = csv.reader(b_file)
    cur.executemany("INSERT INTO Books VALUES (?, ?, ?, ?)", rows)
    con.commit()
    # creates table for all input for authors from the csv file
    cur.execute(
        '''CREATE TABLE Authors(TopBook varchar(255), Author varchar(255), Birth varchar(255), Death varchar(255), Country varchar(255))''')
    con.commit()
    rows = csv.reader(a_file)
    cur.executemany("INSERT INTO Authors VALUES (?, ?, ?, ?, ?)", rows)
    con.commit()

    con.close()


def test_cases():
    # Case for count
    input_lst = ["*", "books"]
    query_db(input_lst)

    # Sample Queries
    # Case for single query
    # Birth <author>
    input_lst = ["Birth", "Authors", "Author", "J.R.R. Tolkien"]
    query_db(input_lst)

    # Case for double query
    # Birth Author <book>
    input_lst = ["Author", "Books", "Book", "1984", "Birth", "authors", "Author"]
    query_db(input_lst)
    input_lst = ["Pages", "Books", "Book", "1984"]
    query_db(input_lst)


def main(args):
    # test cases for code. uncomment to run them
    #test_cases()
    query_db(args)

# lets you run main on its own without being called by parser
#main(["count", "authors"])
