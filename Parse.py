import main as query
import os


# function to split strings
# takes input of a string
def split_sting(user_input):
    quote_occurrences = user_input.count('\"')
    list_of_cmd = []
    if quote_occurrences % 2 != 0:
        print("bad input, mismatched quotes. Ignoring user input")
        return []
    else:
        list_of_strings = user_input.split(" ")
        ind = 0
        while ind < len(list_of_strings):
            # for ind in range(0, len(list_of_strings)):
            word = list_of_strings[ind]
            # search for string that starts with "
            if word != "":
                if word[0] == "\"":
                    # Remove quote from beginning of word
                    list_of_strings[ind] = word[1:]
                    # search for string which occurs after and ends in "
                    end_quote_found = False
                    for word_after_ind in range(ind, len(list_of_strings)):
                        after_word = list_of_strings[word_after_ind]
                        if after_word != "":
                            if after_word[-1] == "\"" and not end_quote_found:
                                # Remove end quote
                                list_of_strings[word_after_ind] = after_word[:-1]
                                string_to_add = ""
                                # make a string which is concatenation of all strings between string that starts with
                                # " and string that ends with "
                                for between_ind in range(ind, word_after_ind + 1):
                                    string_to_add += list_of_strings[between_ind] + " "
                                # remove extra space
                                string_to_add = string_to_add[:-1]

                                # add command to list of command words
                                list_of_cmd.append(string_to_add)

                                # set to true so no other word_after_ind values trigger the containing if statement
                                end_quote_found = True

                                # advance for loop to not double dip
                                ind = word_after_ind
                else:
                    list_of_cmd.append(word)

            ind = ind + 1

        if len(list_of_cmd) != 0:
            if list_of_cmd[-1] == "":
                list_of_cmd.pop(-1)
        return list_of_cmd


# function to general sql command
# takes input of list of commands
def generate_sql_command(user_command_list):
    db_query = []
    length = len(user_command_list)

    if length >= 1:
        first = user_command_list[0]
    else:
        first = ""
    if length >= 2:
        second = user_command_list[1]
    else:
        second = ""


    # These two keywords (authors and books) will return lists of appropriate type
    if first == "authors":
        if second == "with":
            if length >= 3:
                third = user_command_list[2]

                # check if there is a fourth argument
                if length >= 4:
                    fourth = user_command_list[3]
                else:
                    fourth = ""
                    quotes_proper = True

                # if proper quotes (first and last character is a quote, and fourth has more than 1 character)
                # format [field from csv, value in field to search for]

                if third == "country":
                    db_query = ["Author", "Authors", "Country", fourth]
                elif third == "birth":
                    db_query = ["Author", "Authors", "Birth", fourth]
                elif third == "death":
                    db_query = ["Author", "Authors", "Death", fourth]
                elif third == "book":
                    db_query = ["Author", "Authors", "TopBook", fourth]
                else:
                    print("Invalid or missing argument at position 3: " + third)

            else:
                print("missing argument at position 3")
        else:
            print("Invalid or missing argument: at position 2 " + second)
    elif first == "books":
        if second == "with":
            if length >= 3:
                third = user_command_list[2]
                quotes_proper = False
                # check if there is a fourth argument
                if length >= 4:
                    fourth = user_command_list[3]
                else:
                    fourth = ""

                if third == "pages":
                    db_query = ["Book", "Books", "Pages", fourth]
                elif third == "publication":
                    db_query = ["Book", "Books", "Publication", fourth]
                elif third == "author":
                    db_query = ["Book", "Books", "Author", fourth]
                else:
                    print("Invalid or missing argument at position 3: " + third)

        else:
            print("Invalid or missing argument at position 2: " + second)

            # These keywords find value of field specified for book
    elif first == "publication":
        # this case third is the name of a author, whose top-book we want to know about
        if second == "topBook":
            # need to do two queries, so list has length 5, first 3 are one query, second 2 are field and csv of
            # second query
            quotes_proper = False
            # check if there is a third argument
            if length >= 3:
                third = user_command_list[2]
            # if no third argument, use empty string
            else:
                third = ""

            db_query = ["topBook", "Authors", "Author", third, "Publication", "Books", "Book"]
        else:
            # this case second is name of book

            db_query = ["Publication", "Books", "Book", second]

    elif first == "pages":
        # this case third is the name of a author, whose top-book we want to know about
        if second == "topBook":
            # need to do two queries, so list has length 5, first 3 are one query, second 2 are field and csv of
            # second query
            # check if there is a third argument
            if length >= 3:
                third = user_command_list[2]
            # if no third argument, use empty string
            else:
                third = ""

            db_query = ["topBook", "Authors", "Author", third, "Pages", "Books", "Book"]
        else:
            # this case second is name of book
            db_query = ["Pages", "Books", "Book", second]

    elif first == "author":
        # this case second is name of book
        db_query = ["Author", "Books", "Book", second]

    # These keywords find value of field specified for author
    elif first == "birth":
        # this case third is the name of a book, whose author we want to know about
        if second == "author":
            # need to do two queries, so list has length 5, first 3 are one query, second 2 are field and csv of
            # second query
            # check if there is a third argument
            if length >= 3:
                third = user_command_list[2]
            # if no third argument, use empty string
            else:
                third = ""
            db_query = ["Author", "Books", "Book", third, "Birth", "Authors", "Author"]
        else:
            # this case second is name of author
            db_query = ["Birth", "Authors", "Author", second]

    elif first == "death":
        # this case third is the name of a book, whose author we want to know about
        if second == "author":
            # need to do two queries, so list has length 5, first 3 are one query, second 2 are field and csv of
            # second query
            # check if there is a third argument
            if length >= 3:
                third = user_command_list[2]
            # if no third argument, use empty string
            else:
                third = ""
            db_query = ["Author", "Books", "Book", third, "Death", "Authors", "Author"]
        else:
            # this case second is name of author
            db_query = ["Death", "Authors", "Author", second]

    elif first == "topBook":
        # this case third is the name of a book, whose author we want to know about
        if second == "author":
            # need to do two queries, so list has length 5, first 3 are one query, second 2 are field and csv of
            # second query
            # check if there is a third argument
            if len(user_command_list) >= 3:
                third = user_command_list[2]
            # if no third argument, use empty string
            else:
                third = ""
            db_query = ["Author", "Books", "Book", third, "Popular", "Authors", "Author"]
        else:
            # this case second is name of author
            db_query = ["TopBook", "Authors", "Author", second]

    elif first == "country":
        if second == "author":
            # need to do two queries, so list has length 5, first 3 are one query, second 2 are field and csv of
            # second query
            # check if there is a third argument
            if length >= 3:
                third = user_command_list[2]
            # if no third argument, use empty string
            else:
                third = ""
            db_query = ["Author", "Books", "Book", third, "Country", "Authors", "Author"]
        else:
            # this case second is name of author
            db_query = ["Country", "Authors", "Author", second]
    # returns a count of entries in a csv
    elif first == "count":
        if second == "authors":
            db_query = ["count", "authors"]
        elif second == "books":
            db_query = ["count", "books"]

    # signals outer function to quit program
    elif first == "quit":
        return ["quit"]
    elif first == "":
        print("no command entered")
    else:
        print("not a valid keyword or command at position 1: " + first)

    return db_query


def user_input_loop():
    # first boolean keeps track of if first user query has happened
    # second keeps track of if user wants to continue entering commands
    first_query_happened = False
    keep_going = True

    while keep_going:
        # get user input
        cmd = input("enter command: ")

        # quits program
        if cmd == "quit":
            keep_going = False
            query.db_quit()
            print("Exiting program...")

        # prints helpful info for all commands
        elif cmd == "help":
            try:
                command_file = open("commands.txt")
            except FileNotFoundError:
                print("Missing commands.txt file")
                print("Exiting....")
                exit()
            print("this is the help command \n")
            print("below is a list of all possible commands \n")
            command_lines = command_file.readlines()
            for line in command_lines:
                print(line, end="")
            print("\n")
            command_file.close()

        # loads data. notifies user if it is already loaded
        elif cmd == "load data":
            if os.path.exists("books_and_authors.db"):
                print("data is already loaded")
            else:
                query.create_db()
                print("data loaded")

        else:

            # note that connection is initialized even if user query (cmd) is invalid
            if os.path.exists("books_and_authors.db"):
                # this if keeps track of whether first query has occurred. If it hasn't, we notify main
                # by sending a list of length one, which will initialize connection to database. WE then proceed
                # to send main a db_query, and on subsequent queries just send db_query,
                # as connection has already been opened
                if not first_query_happened:
                    first_query_happened = True
                    query.main([first_query_happened])
                    print("Connection initialized")

                # process user input and send to main
                format_cmd = split_sting(cmd)
                db_query = generate_sql_command(format_cmd)
                if len(db_query) != 0:
                    query.main(db_query)
                print("")
            else:
                print("Error database not loaded")
                print("Please load the database with the load data command")
                print("before attempting any other queries")


user_input_loop()
